# calc
Simple command line calculator

## How to use
```
python calc.py "123+123"
python calc.py "3(7)"
python calc.py "4(7/(1+1))(9+2)"
```

## Tests
```
python tests\parser.py
python tests\tools.py
```

## Improvements / Todo List
* Add more operators. Only + - / * and () are currently available.
  Idea list: square root, log, ln, e(), ^
* Include errors in tests.
* More tests.
