import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from src.Parser import Parser


class MyTestCase(unittest.TestCase):
    def test(self):
        # Sums

        # "9+9" => 18
        value = Parser("9+9").calc()
        self.assertEqual(value, 18)

        # "1+2+3+4+5+6 +7+8       +           9" => 45
        value = Parser("1+2+3+4+5+6 +7+8       +           9").calc()
        self.assertEqual(value, 45)

        # Subtractions

        # "1-2-3-4-5-6 -7-8       -           9" => -43
        value = Parser("1-2-3-4-5-6 -7-8       -           9").calc()
        self.assertEqual(value, -43)

        # Multiplication

        # "1*2" => 2
        value = Parser("1*2").calc()
        self.assertEqual(value, 2)

        # "1*2*3*4*5*6*7*8*9" => 362880
        value = Parser("1*2*3*4*5*6*7*8*9").calc()
        self.assertEqual(value, 362880)

        # Divisions

        # "1/1" => 1
        value = Parser("1/1").calc()
        self.assertEqual(value, 1)

        # "98989898989898/98" => 1010101010101.0
        value = Parser("98989898989898/98").calc()
        self.assertEqual(value, 1010101010101.0)

        # Complex operations

        # "11*((9+9)+2+1) + 4    -6" => 229
        value = Parser("11*((9+9)+2+1) + 4    -6").calc()
        self.assertEqual(value, 229)

        # "11((9+9)+2+1) + 4    -6" => 229
        value = Parser("11((9+9)+2+1) + 4    -6").calc()
        self.assertEqual(value, 229)

        # "2(((((4)) + 1)) * 2)" => 20
        value = Parser(" 2*(((((4))+1))*2)").calc()
        self.assertEqual(value, 20)


if __name__ == '__main__':
    unittest.main()

