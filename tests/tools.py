import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from src.tools import *


class MyTestCase(unittest.TestCase):
    def test_something(self):

        value = str_to_numeric('123')
        self.assertEqual(isinstance(value, int), True)
        self.assertEqual(value, 123)

        value = str_to_numeric('123.0')
        self.assertEqual(isinstance(value, float), True)
        self.assertEqual(value, 123.0)

        value = str_to_numeric('9791698484763842849874089592106752.574176517981765176502175649112759076502179581978179')
        self.assertEqual(isinstance(value, float), True)
        self.assertEqual(value, 9791698484763842849874089592106752.574176517981765176502175649112759076502179581978179)


if __name__ == '__main__':
    unittest.main()
