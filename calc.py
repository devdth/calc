import argparse

from src.Parser import Parser


# Command line options parser: argparse
parser = argparse.ArgumentParser()
parser.add_argument("operation", help="Example \"1+1\"")
args = parser.parse_args()

# Process mathematical operation
parser = Parser(args.operation)
operation_result = parser.calc()

# Display result
print(operation_result, end = '')
