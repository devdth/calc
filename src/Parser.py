from src.enum.ElementType import ElementType
from src.tools import str_to_numeric, error


class Parser:
    def __init__(self, operation):
        self.index = 0
        self.operation = operation
        self.last_element_type = ElementType.NONE

        # Clean the user input. Delete all whitespaces
        self.operation = operation.replace(' ', '')
        # Get the string's length
        self.length = len(self.operation)

        # Stack for numbers
        self.numeric_stack = []
        # Stack for operators
        self.operator_stack = []

    def calc(self):
        # Starts the processing and returns the value

        value, index = self.process_operation(0)
        return value

    def process_operation(self, index):
        # Get two values: the operation's 'result' and the new 'index'

        self.index = index
        # Parse the mathematical operation and stop when a closing parenthesis is found
        while self.index < self.length and self.operation[self.index] != ')':

            # There's a new character to read.
            self.analyze_content()

            # Increment. Go to the next character and analyze it.
            self.index += 1

        # Calculate all low priority remaining operations at the end (ex: + and -).
        result = self.process_low_priority_operator()
        return result, self.index

    def analyze_content(self):
        # Analyze the current character and decide what kind of processing will be done.
        # High priority operators will be processed as soon as possible.
        # Low priority operators and their related values will be stored in a stack. They will wait until the end to be
        # processed

        if self.operation[self.index].isdigit():  # Number
            if self.last_element_type == ElementType.OPERAND:
                error("An operation have an operator between two operands")
            self.analyze_number()
        elif self.operation[self.index] == '(':
            self.analyze_parenthesis()
        else:
            if self.last_element_type == ElementType.NONE:
                error("An operation must start with an operand")
            if self.last_element_type == ElementType.HIGH_PRIORITY_OPERATOR or self.last_element_type == ElementType.LOW_PRIORITY_OPERATOR:
                error("An operation cannot have two trailing operators")

            if self.operation[self.index] == '+':
                self.operator_stack.append('+')
                self.last_element_type = ElementType.LOW_PRIORITY_OPERATOR
            elif self.operation[self.index] == '-':
                self.operator_stack.append('-')
                self.last_element_type = ElementType.LOW_PRIORITY_OPERATOR
            elif self.operation[self.index] == '*':
                self.analyze_multiplication()
            elif self.operation[self.index] == '/':
                self.operator_stack.append('/')
                self.last_element_type = ElementType.HIGH_PRIORITY_OPERATOR
            else:
                error("Unknown character, please check the 'operation' arg")

    def analyze_number(self):
        # A number can be an int or a float.

        value = self.get_number()
        self.numeric_stack.append(str_to_numeric(value))

        if self.last_element_type == ElementType.HIGH_PRIORITY_OPERATOR:
            self.process_high_priority_operator()

        self.last_element_type = ElementType.OPERAND

    def analyze_parenthesis(self):
        # An open parenthesis is considered as a new operation. It's content will be processed recursively.

        if self.last_element_type == ElementType.OPERAND:
            # case x(y) = x*y
            self.analyze_multiplication()

        parse = Parser(self.operation)
        value, self.index = parse.process_operation(self.index + 1)
        self.numeric_stack.append(value)

        if self.last_element_type == ElementType.HIGH_PRIORITY_OPERATOR:
            self.process_high_priority_operator()

        self.last_element_type = ElementType.OPERAND

    def analyze_multiplication(self):
        self.operator_stack.append('*')
        self.last_element_type = ElementType.HIGH_PRIORITY_OPERATOR

    def get_number(self):
        # Parse the string and returns the whole number as an int or a float.

        number = ''
        while self.index < self.length and (self.operation[self.index].isdigit() or self.operation[self.index] == '.'):
            number = number + self.operation[self.index]
            self.index += 1
        # Don't go too far. This is already the character after the current number. We will process that later.
        self.index -= 1
        return number

    def process_low_priority_operator(self):
        # Low priority operators have to be processed as a batch and last.
        # Returns the results and empties both value and operator stacks

        if len(self.numeric_stack) == 0:
            return 0

        stack_result = self.numeric_stack.pop(0)
        while len(self.numeric_stack) > 0:
            operator = self.operator_stack.pop(0)
            next_value = self.numeric_stack.pop(0)
            if operator == '+':
                stack_result = stack_result + next_value
            elif operator == '-':
                stack_result = stack_result - next_value
            else:
                error("Please check the 'operation' arg")
                exit()
        return stack_result

    def process_high_priority_operator(self):
        # A high priority operator will be processed alone.

        stack_result = self.numeric_stack.pop(0)
        operator = self.operator_stack.pop(0)
        next_value = self.numeric_stack.pop(0)
        if operator == '*':
            stack_result = stack_result * next_value
        elif operator == '/':
            if next_value == 0:
                error("Can't divide by 0")
            stack_result = stack_result / next_value
        else:
            error("Please check the 'operation' arg")
            exit()
        # push the result in the list
        self.numeric_stack.append(stack_result)
