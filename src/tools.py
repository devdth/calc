def str_to_numeric(string_numeric):
    # Returns a converted string numeric value as an int or a float

    if string_numeric.find('.') == -1:
        return int(string_numeric)
    else:
        return float(string_numeric)


def error(string):
    # Prints an error and exits the program
    print(string, end = '')
    exit()
