from enum import Enum


class ElementType(Enum):
    NONE = 0
    OPERAND = 1
    LOW_PRIORITY_OPERATOR = 2
    HIGH_PRIORITY_OPERATOR = 3
